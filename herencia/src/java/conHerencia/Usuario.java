package conHerencia;

public class Usuario {

    protected String nombre;
    protected String usuario;
    protected String clave;

    public void login() {
    }

    public void logout() {
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", usuario=" + usuario + ", clave=" + clave + '}';
    }

}
